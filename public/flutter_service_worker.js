'use strict';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "/index.html": "6a709bbcd250f2cbc2238b95925725fc",
"/main.dart.js": "fb5531f1a0bf7719710e1672a71b3304",
"/icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"/icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"/manifest.json": "73974f27b05f20b4cb13d13177ec9f50",
"/assets/LICENSE": "12711fe300afe9392330131034af82da",
"/assets/AssetManifest.json": "130a98fd299f24c405191b1b5a1e2492",
"/assets/FontManifest.json": "a4bf12eb5b027a074bfdd71d0ded3ec0",
"/assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "115e937bb829a890521f72d2e664b632",
"/assets/fonts/Suplexmentary%20Comic%20NC.ttf": "3f0f1802550efef2948b10e59d34a881",
"/assets/fonts/RalewayDots-Regular.ttf": "39d3174a2785af3a8f178f9a11dd8ac0",
"/assets/fonts/MaterialIcons-Regular.ttf": "56d3ffdef7a25659eab6a68a3fbfaf16"
};

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheName) {
      return caches.delete(cacheName);
    }).then(function (_) {
      return caches.open(CACHE_NAME);
    }).then(function (cache) {
      return cache.addAll(Object.keys(RESOURCES));
    })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        if (response) {
          return response;
        }
        return fetch(event.request, {
          credentials: 'include'
        });
      })
  );
});
